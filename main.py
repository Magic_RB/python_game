import os
import ctypes

os.environ["PYSDL2_DLL_PATH"] = "./SDL2.dll"

import OpenGL
from sdl2 import *

running = True

SDL_Init(SDL_INIT_VIDEO)

window = SDL_CreateWindow(b"Hello", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1080, 1080, SDL_WINDOW_SHOWN)

event = SDL_Event()

while running:
    while SDL_PollEvent(ctypes.byref(event)) != 0:
        if event.type == SDL_QUIT:
            running = False
            break

SDL_DestroyWindow(window)
SDL_Quit()

